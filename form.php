<!DOCTYPE html>
<html lang="ru">

  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>index</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <div id="form">
      <h2>Форма.</h2>
      <form action="index.php" method="POST">
        <?php 
          if (!empty($messages))
          {
            print('<div id="messages">');
            foreach ($messages as $message)
            {
              print($message);
            }
            print('</div>');
          }
        ?>
        Имя (максимум 128 символов):<br>
        <input type="text" name="name"
        <?php 
          if ($errors['name'])
          {
            print 'class="error"';
          }
        ?>
          value="
        <?php 
          print $values['name']; 
        ?>"><br>

        E-male (максимум 128 символов):<br>
        <input type="email" name="email"
        <?php 
          if($errors['email'])
          {
            print 'class="error"';
          }
        ?>
        value="
        <?php
          print $values['email']; 
        ?>"><br>

        Год рождения:<br>
        <select name="year" <?php 
          if($errors['year'])
          {
            print 'class="error"';
          }
        ?>
        value="
        <?php
          print $values['year']; 
        ?>">
          <option value="2000">2000</option>
          <option value="2001">2001</option>
          <option value="2002">2002</option>
          <option value="2003">2003</option>
          <option value="2004">2004</option>
          <option value="2005">2005</option>
          <option value="2006">2006</option>
          <option value="2007">2007</option>
          <option value="2008">2008</option>
          <option value="2009">2009</option>
          <option value="2010">2010</option>
          <option value="2011">2011</option>
          <option value="2012">2012</option>
          <option value="2013">2013</option>
          <option value="2014">2014</option>
          <option value="2015">2015</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
        </select><br>
        Пол:<br>
        <input type="radio" value="1" checked="checked" name="sex"> Мужской
        <input type="radio" value="2" name="sex"> Женский<br>
        Количество коненостей:<br>
        <input type="radio" name="limb" value="0"> 0
        <input type="radio" name="limb" value="1"> 1
        <input type="radio" name="limb" value="2"> 2
        <input type="radio" name="limb" value="3"> 3
        <input type="radio" name="limb" checked="checked" value="4"> 4
        <input type="radio" name="limb" value="5"> 5
        <input type="radio" name="limb" value="6"> 6
        <input type="radio" name="limb" value="7"> 7
        <input type="radio" name="limb" value="8"> 8
        <input type="radio" name="limb" value="9"> 9
        <input type="radio" name="limb" value="10"> 10<br>
        Сверхспособности:<br>
        <select name="superpowers[]" multiple="multiple">
          <option value="immortal">Бессмертие</option>
          <option value="intangible">Прохождение сквозь стены</option>
          <option value="fly">Левитация</option>
          <option value="ability6">Способность представить 6-ти мерное пространство</option>
        </select><br>
        Биография (максимум 500 символов):<br>
        <textarea name="biography"></textarea><br>
        <input type="checkbox" name="agree">С контрактом ознакомлен.<br>
        <input type="submit" value="Отправить">
      </form>
    </div>

    <footer>
      <h2>(c) Дмитрий Щербин</h2>
    </footer>
  </body>
</html>
