<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
  $messages = array();

  if (!empty($_COOKIE['save']))
  {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['superpowers'] = !empty($_COOKIE['spw_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['agree'] = !empty($_COOKIE['agree_error']);
  
  if ($errors['name'])
  {
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="error">Исправте введенное имя.<br></div>';
  }

  if ($errors['email'])
  {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Исправте введенный email.<br></div>';
  }

  if ($errors['year'])
  {
    setcookie('year_error', '', 100000);
    $messages[] = '<div class="error">Выберете год.<br></div>';
  }

  if ($errors['sex'])
  {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error">Выберете пол.<br></div>';
  }

  if ($errors['limb'])
  {
    setcookie('limb_error', '', 100000);
    $messages[] = '<div class="error">Выберете количество конечностей.<br></div>';
  }

  if ($errors['superpowers'])
  {
    setcookie('spw_error', '', 100000);
    $messages[] = '<div class="error">Выберете суперспособности.<br></div>';
  }

  if ($errors['biography'])
  {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error">Введите свою биографию.<br></div>';
  }
  
  if ($errors['agree'])
  {
    setcookie('agree_error', '', 100000);
    $messages[] = '<div class="error">Подтвердите, что вы согласны.<br></div>';
  }

  $values = array();

  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['superpowers'] = empty($_COOKIE['spw_value']) ? '' : $_COOKIE['spw_value'];
  $values['limbs'] = empty($_COOKIE['limb']) ? '' : $_COOKIE['limb_value'];
  $values['biography'] = empty($_COOKIE['biography']) ? '' : $_COOKIE['biography_value'];
  $values['submit'] = empty($_COOKIE['submit']) ? '' : $_COOKIE['submit_value'];

  include('form.php');
  //exit();
}

else
{
  $errors = FALSE;
  
  if (empty($_POST['name']) || !(preg_match("/^[a-z0-9_-]{2,20}$/i", $_POST['name'])))
  {
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['email']) || !preg_match("/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/", $_POST['email']))
  {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['sex']))
  {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['year']))
  {
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['limb']))
  {
    setcookie('limb_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['superpowers']))
  {
    setcookie('spw_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('spw_value', $_POST['superpowers'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['biography']))
  {
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['agree']))
  {
    setcookie('agree_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else
  {
    setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
  }
  
  if ($errors)
  {
    header('Location: index.php');
    exit();
  }
  else
  {
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limb_error', '', 100000);
    setcookie('spw_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('agree_error', '', 100000);
  }
  
  try
  {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $year = $_POST['year'];
    $sex = $_POST['sex'];
    $limb = $_POST['limb'];
    $spw = $_POST['superpowers'];
    $biography = $_POST['biography'];
  
    $db = new PDO('mysql:host=localhost;dbname=u35658', 'u35658', '3463246', array(PDO::ATTR_PERSISTENT => true));
  
    $stmt = $db->prepare("INSERT INTO `users` (`name`, `emale`, `year`, `sex`, `limb`, `biografy`) VALUES (:name, :email, :year, :sex, :limb, :biography)");
    $stmt -> execute(array(':name' => $name, ':email' => $email, ':year' => $year, ':sex' => $sex, ':limb' => $limb, ':biography' => $biography));
  
    $stmt = $db->prepare("SELECT `id` FROM `users` WHERE name = :name AND emale = :email");
    $stmt->execute(array(':name' => $name, ':email' => $email));
    $userid = $stmt->fetchAll();
  
    $spwid = array();
  
    for($i=0; $i<count($spw); $i++)
    {
      $stmt = $db->prepare("SELECT `spwid` FROM `spwdescriptor` WHERE naveofspw = :spw");
      $stmt ->execute(array(':spw' => $spw[$i]));
      $spwid[$i] = $stmt->fetchColumn();
    }
    
    for($i=0; $i<count($spw); $i++)
    {
      $stmt = $db->prepare("INSERT INTO `spwids` VALUES (:userid, :spwid)");
      $stmt ->execute(array(':userid' => $userid[0]["id"], ':spwid' => $spwid[$i]));
    }
  
  }
  catch(PDOException $e)
  {
    print('Error : ' . $e->getMessage());
    exit();
  }
  
  setcookie('save', '1');
  header('Location: index.php');
}
?>
